Contributions by Andrew M.A. Cater

via irc, 10 - 14 sept 2023

(For Noodles and all those there)

For Debconf two three in Kochi
People come from far and wide to see
All in Kerala state
But it's sad to relate
That one who is *not* there is me

The State rules on booze are quite clear
And are those to which you must adhere
Public drinking own liquor
Will bring down sanctions quicker
You must buy in the hotel, my dear


(The video team)

The video team's in a stew
They've so much more now to review
Don't tell them to start faster
They've committed to master
The fix that they struggled to view

This limerick writing's a must
And cheaper than drugs, booze or lust
For better or worse
With five lines to each verse
I'll finish another or bust!


(This one was written as social media messages to my daughter line by line)

 For my daughter

 Even if the world is out to get you,
   I'll still love you
 Even if the rain is out to wet you
   I'll still love you
 Even when the evening sun is set
   I'll still love you
 If you can't guess the next rhyme (I bet)
   I'll still love you

 Even if the world gets you upset,
   I'll still love you
 Even if the laptop falls off the 'Net,
   I'll still love you
 When each day can't really better get,
   I'll still love you

Technical love poem

If, unexpectedly, I hear your voice
My heart stops, leaps and strains to cross the void
An instantaneous frequency response
Reflex emotions' plot is cardioid

(Not quite) Twinkle Twinkle little star

Ah ! Vous dirai-je maman
Ce qui cause mon tourment?
Papa veut que je raisonne
Comme une grande personne
Moi je dis que les bonbons
Valent mieux que la raison.

translation in english:

Ah, now let me tell you, Mummy
What's upset my head and tummy
Daddy wants me to think through
Like a grown-up - well, would you?
Me? - I think that sweeties are
Worth more than such thought by far

and another translation:

 Ah, now I will tell you, Mom
What's made me lose my aplomb
Daddy wants, as end result,
I should think like an adult
Me, I think that pretty sweeties
Are worth more than such entreaties


The poem joostvb would like to do is "Lof van het onkruid" which starts with the line "Godlof dat onkruid niet vergaat." by dutch poet Ida Gerhardt  ( ★ 1905 – † 1997)  , published 1974.  She did a PhD on the work De Rerum Natura by Roman poet and philosopher Lucretius and worked as a teacher.  She lived together with a female friend.    Some of her works are translated to German.  The poem is about "The weeds win the last fight";  it
speaks to me since I'm worried about the climate crisis; and I'm imagining what the scenarios are which blah might happen in our future.

highvoltage read:
    https://www.poetryfoundation.org/poems/51296/ithaka-56d22eef917ec

an encore by Andrew M.A. Cater: the Debian social contract as a limerick

 Debian will *always* be free
 Give back to the community
 We won't hide problems and we care
 For users *and* for free software
 If it's too hard - well there's non free
 Firmware in the installer, see?



 contribution by urbec:

This debconf
-------------
Walking on a twisted path,
opening doors, not knowing, what's behind them.
Opening the right ones, but also the wrong ones.
Truth is not simple, it's twisted.
Things are different then they seem to be.
More about our own existance revealed
than we ever would have been willing to accept.
Learning to distinguish right from wrong,
important from unimportant.

Continuing the path,
finding things long lost.
Realising, how much could be found,
if it all just could continue.
If it all would never end.

There is only one thing,
I know for sure:

I DON'T WANT TO GO.


and urbec did the one which is online at https://genius.com/Ren-bittersweet-symphony-the-verve-retake-lyrics
