First of all, the most important Debconf (photo) event will be happening *Saturday*, at no later than 12:30. Please be ready to go *directly* from the previous talk rooms or hacklabs to the photo taking spot when volunteer ushers will ask you to do so, which will happen at the end of the preceding talks, so at around 12:15.

We will then all orderly proceed to the *steps just outside the main talk room* (Anamudi), which is just a  5 minute walk from the hotel (in case you have not been there yet). Follow the crowds or the map inside your badge.

When I know that everyone has arrived, but no later than 12:30, I will start taking the many pictures needed to assemble the Group Photo. Please try to keep looking forward, to me, without changing your head position during that time, even if it looks like I am photographing something else. Blinking is permitted. Manually correcting zombie heads on stitching boundaries is really time consuming :)

After the main group photo, we will also do the traditional t-shirts photo, so if you brought rare Debconf t-shirts, put them on or bring them with you so we can put them on other, yet unsuspecting people. https://wiki.debian.org/DebConf/23/DebconfShirts

As an additional photo event, celebrating the fact that, after loooong and dry 17 years, we finally have another venue with a pool, we will be attempting another Pool Group Photo, like we did in Debconf6. That will happen on Saturday at 14:00, during lunch break. Bring your swimsuits.





