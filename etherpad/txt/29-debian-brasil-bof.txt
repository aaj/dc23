Bem-vindas e bem-vindos ao BoF do Debian Brasil

Alguns tópicos para discussão:
    - MiniDebConf Belo Horizonte 2024
	   - Conversa com um professor da UFMG
	   - Vai ocorrer por meio de um projeto dentro da universidade (projeto de extensão)
	   - Vantagem de fazer na universidade seria facilidade de chamar os estudantes
	   - Datas: 2, 3 e 4 de maio 2024 quarta-feira (1 maio) é feriado
	   - Levantar todas as areas que precisam para iniciar o evento (Esperando a aprovação da UFMG)
	   - Inscrição em MiniDebConf com pagamento para quem quiser ajudar no evento
	   - Montar times: bolsas, site, conteúdo, divulgação, inscrições (front desk), patrocínio
	   - Verificar com os adms do peertube.debian.social para habilitar o stream (testar em um evento beta)
	   - Enfrentamos problemas de captura de tela, ter equipamentos mínimos para garantir uma boa qualidade
	   - Focar em melhorar a captura de tela (ver spliter do time de vídeo)
        - Wafer
        - Bolsas para participantes - definir valor total e pedir ao DPL
        - Video - gravação e transmissão
    -  Outras MiniDebConfs no Brasil?
       - No inicio achamos melhor deixar apenas 1 por ano, mas quem sabe no futuro aumentar o número
       - Uma que todos do brasil iriam e outras mais locais
       - Ter um grupo para ficar responsável e tocar esses tipos de eventos
       - Grupo BOSS (voltado a minorias), conversar com eles para movimentar

    (Lenharo): Acredito que focar em 1 por ano seja OK... e focar em participar em outros eventos (e aumentar a participação da comunidade nesses eventos - como: Latinoware, Campus, cryptorave)
    - MiniDebConf LatAm 2024
        - No Uruguai em 2024 (?)
        - Santiago está organizando um evento
        - Organizando uma microdebconf em Uruguai em novembro 9-11
        - Vai ser focado em pessoas que estão pela região
        - Não vai ter tempo suficiente para organizar alguma coisa maior
        - No futuro gostaria de organizar uma MiniDebConf America (Pode ser em novembro também, e está aberto a definir mais coisas)
        - Esse ano uma menor e ano que vem uma maior
        - Pessoal do brasil está disposto a ajudar IRC #debian-latam (Basta entrar em contato)
        - Depois itinerante
    - Reunião de contribuidores para trabalhar no Debian
        - São Paulo?
        - Fazer um evento em algum lugar acessível
        - Trabalhar em pacotes e bugs
        - Normalmente nas MiniDebConf perdemos muito tempo ajudando pessoas e ensinando novas pessoas
        - Essa reunião não teria palestra, apenas mão na massa
        - Seria algo parecido com a MIniDebCong Humburgo
        - Não seria exclusivo para DDs ou DMs mas tem que ter conhecimento de empacotamento
        - Depende do DPL aprovar um orçamento para isso
    - Como encorajar comunidades locais / como ajudar as comunidades
        - Buscar pessoas não tecnicas
        - Alguma forma de transmitir informações mais simples
        - Paulo criou o debian nordeste (Incentivar pessoas a fazer debian days e futuras debconfs)
        - Falta pessoas se voluntariarem e compremeterem a organizar o evento
        - Ver como podemos ajudar e instruir as pessoas
        - Ter guidelines de como criar grupos locais (Documentado de preferência)
        - Exemplos de guidelines ("Como posso fazer e organizar um debian day em minha cidade")
        - Bolsa para trazer essas pessoas com interesse em trazer para eventos maiores
        - Meta para debian day de talvez 10, esse ano tivemos 7 lugares que organizaram
        - Ir em escolas, universidade e tentar arrumar um espaço para falar sobre o projeto
        - Track em minidebconf voltado ao interesse da pessoas, e.g arquiteto, musico, etc
        - Estrategia de chegar em pessoas que não conhecem sobre o projeto ou grupos locais
    - Atividades online no canal Debian Brasil no YouTube
        - 
    - Pesquisa sobre o uso do Debian no Brasil e os (as) usuários(as). Modelo https://blog.nilo.pro.br/posts/2023-05-08-python-no-brasil-2023/

    Valeu pessoal!
    ---
    - 
