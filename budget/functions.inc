; Calculate Indian GST at the specified rate
; Deduct 2% TDS to be paid directly as tax
= expr "account =~ /^expenses:.*/ and has_meta('GST') and not has_tag('GST-deferred')"
    $account                (amount * tag("GST") / 100) ; GST
    assets:foss-united      (amount * tag("GST") / 100 * -1)
    assets:foss-united                    (amount * 0.02) ; TDS
    liabilities:deferred-tax:foss-united  (amount * 0.02 * -1)

; Expect Indian GST at the specified rate and defer it
; Deduct 2% TDS to be paid directly as tax
= expr "account =~ /^expenses:.*/ and has_meta('GST') and has_tag('GST-deferred')"
    $account                              (amount * tag("GST") / 100) ; GST (deferred)
    liabilities:deferred-tax:foss-united  (amount * tag("GST") / 100 * -1)
    assets:foss-united                    (amount * 0.02) ; TDS
    liabilities:deferred-tax:foss-united  (amount * 0.02 * -1)
